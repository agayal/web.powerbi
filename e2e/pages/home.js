module.exports = {
  url: 'http://localhost:3000',
  elements: {
    body: {
      selector: 'body',
    },
    trainingButton: {
      selector: 'button[id=training]',
    },
    myPageButton: {
      selector: 'button[id=mypage]',
    },
  },
};
