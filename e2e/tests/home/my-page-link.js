module.exports = {
  'Check mypage button': (browser) => {
    browser
      .url('http://localhost:3000')
      .waitForElementVisible('button[id=mypage]', 1000)
      .assert.containsText('button[id=mypage]', 'MY PAGE')
      .click('button[id=mypage]')
      .pause(2000)
      .assert.containsText('body', 'Start coding here ...')
      .end();
  },
};
