module.exports = {
  'Check training button': (client) => {
    const home = client.page.home();
    home
      .navigate()
      .assert.containsText('@body', 'Web.App.PowerBI')
      .waitForElementVisible('@trainingButton', 1000)
      .assert.containsText('@trainingButton', 'TRAINING')
      .click('@trainingButton');

    client.pause(2000);

    home.assert.containsText('@body', 'Your name is:');

    client.end();
  },
};
