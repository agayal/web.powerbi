module.exports = function() {
  const presets = [
    ['@babel/preset-env', { modules: false }],
    '@babel/preset-react',
  ];

  const plugins = [
    'babel-plugin-typescript-to-proptypes',
    ['@babel/plugin-transform-typescript', { isTSX: true }],
    '@babel/plugin-proposal-class-properties',
  ];

  return {
    presets,
    plugins,
  };
};
