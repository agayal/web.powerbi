module.exports = {
  root: true,
  extends: '@dealersocket/ds-react-typescript',
  rules: {
    // Rules are defined in: @dealersocket/eslint-config-ds-react-typescript
    // Avoid adding or overriding rules here
    //
    // https://github.com/bradzacher/eslint-plugin-typescript/issues/290
    // use rules from :@typescript-eslint/eslint-plugin
    // https://www.npmjs.com/package/@typescript-eslint/eslint-plugin
  },
};
