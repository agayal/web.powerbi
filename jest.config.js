/* eslint-disable prettier/prettier */
module.exports = {
  collectCoverageFrom: [
    // 'src/**/*.js',
    '!src/**/mock/**',
    '!**/mocks/**',
    '!src/mock-adapter.ts',
    '!src/**/*.stories.ts',
  ],

  coverageThreshold: {
    global: {
      statements: 95,
      branches: 95,
      functions: 95,
      lines: 95,
    },
  },

  coverageReporters: [
    // 'json',
    // 'lcov',
    'text',
    'html',
    // 'text-summary',
  ],

  coverageDirectory: './coverage',

  moduleDirectories: ['node_modules', 'src'],

  moduleNameMapper: {
    '^.+\\.(css|scss)$': 'identity-obj-proxy',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/file-mock.js',
  },

  // from: CRA
  moduleFileExtensions: [
    'web.js',
    'js',
    'web.ts',
    'ts',
    'web.tsx',
    'tsx',
    'json',
    'web.jsx',
    'jsx',
    'node',
  ],

  // from: CRA
  // resolver: 'jest-pnp-resolver',

  setupFiles: [
    // from: CRA
    'react-app-polyfill/jsdom',
    './config/jest/jest.setup.js',
  ],

  // Add options to global test config to simplify setup and teardown of tests
  setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect'],

  // convert Enzyme wrappers to format for snapshot testing
  snapshotSerializers: ['enzyme-to-json/serializer'],

  // from: CRA
  testEnvironment: 'jsdom',

  // pattern uses to find the test files
  testRegex: 'src/.*\\.(test|spec)\\.(js|ts|tsx)$',

  testURL: 'http://localhost',

  // from: CRA
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': '<rootDir>/node_modules/babel-jest',
    '^.+\\.css$': '<rootDir>/config/jest/cssTransform.js',
    '^(?!.*\\.(js|jsx|ts|tsx|css|json)$)':
      '<rootDir>/config/jest/fileTransform.js',
  },

  transformIgnorePatterns: [
    // from: CRA
    // '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$',
    // from: CRA
    // '^.+\\.module\\.(css|sass|scss)$',
    'node_modules/(?!(@dealersocket/ds-ui-react)/)',
  ],
};
