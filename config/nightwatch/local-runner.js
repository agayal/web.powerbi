#!/usr/bin/env node

const Nightwatch = require('nightwatch');
const browserstack = require('browserstack-local');

let bsLocal;

try {
  process.mainModule.filename = './node_modules/.bin/nightwatch';

  // Code to start browserstack local before start of test
  console.log('Connecting local');
  Nightwatch.bs_local = bsLocal = new browserstack.Local();
  bsLocal.start({ key: process.env.BROWSERSTACK_KEY }, function (error) { // eslint-disable-line
    if (error) throw error;

    console.log('Connected. Now testing...');
    Nightwatch.cli(function (argv) { // eslint-disable-line

      Nightwatch.CliRunner(argv)
        .setup({}, function () { // eslint-disable-line
          // Code to stop browserstack local after end of parallel test
          bsLocal.stop(function () { }); // eslint-disable-line
        })
        .runTests(function(){ // eslint-disable-line
          // Code to stop browserstack local after end of single test
          bsLocal.stop(function () { }); // eslint-disable-line
        });
    });
  });
} catch (ex) {
  console.log('There was an error while starting the test runner:\n\n');
  process.stderr.write(`${ex.stack} \n`);
  process.exit(2);
}
