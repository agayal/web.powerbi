module.exports = {
  src_folders: ['e2e/tests'],
  page_objects_path: 'e2e/pages',
  output_folder: 'e2e/reporting',
};
