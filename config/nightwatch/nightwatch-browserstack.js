const baseConfig = require('./base.config.js');
const argv = require('minimist')(process.argv);

// do we want to use browserstack local?
// if yes pass arg --local
const local = argv.local || false;

const config = Object.assign(baseConfig, {
  selenium: {
    start_process: false,
    host: 'hub-cloud.browserstack.com',
    port: 80,
  },

  // These are the three most popular os/browser versions our clients use
  // http://kibana.dealersocket.net/goto/4a4ca1eac55791589fdc7213b64c9f16
  test_settings: {
    chrome_win7: {
      desiredCapabilities: {
        os: 'Windows',
        os_version: '7',
        browser: 'Chrome',
        browser_version: '57',
        'browserstack.user': process.env.BROWSERSTACK_USERNAME || 'BROWSERSTACK_USERNAME',
        'browserstack.key': process.env.BROWSERSTACK_KEY || 'BROWSERSTACK_KEY',
        'browserstack.local': local,
      },
    },
    chrome_win10: {
      desiredCapabilities: {
        os: 'Windows',
        os_version: '10',
        browser: 'Chrome',
        browser_version: '57',
        'browserstack.user': process.env.BROWSERSTACK_USERNAME || 'BROWSERSTACK_USERNAME',
        'browserstack.key': process.env.BROWSERSTACK_KEY || 'BROWSERSTACK_KEY',
        'browserstack.local': local,
      },
    },
    internet_explorer_11_win7: {
      desiredCapabilities: {
        os: 'Windows',
        os_version: '7',
        browser: 'ie',
        browser_version: '11.0',
        'browserstack.user': process.env.BROWSERSTACK_USERNAME || 'BROWSERSTACK_USERNAME',
        'browserstack.key': process.env.BROWSERSTACK_KEY || 'BROWSERSTACK_KEY',
        'browserstack.local': local,
      },
    },
  },
});

for (const i in config.test_settings) { // eslint-disable-line
  const environment = config.test_settings[i];
  environment.selenium_host = config.selenium.host;
  environment.selenium_port = config.selenium.port;
}

module.exports = config;
