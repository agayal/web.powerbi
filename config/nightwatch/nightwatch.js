const argv = require('minimist')(process.argv);
const baseConfig = require('./base.config.js');
const drivers = require('./drivers.js');

const platform = argv.platform || 'windows';

const config = Object.assign(baseConfig, {
  selenium: {
    start_process: true,
    server_path: 'node_modules/selenium-server-standalone-jar/jar/selenium-server-standalone-3.3.1.jar',
    cli_args: drivers[platform],
  },

  test_settings: {
    default: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
      },
    },
    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true,
      },
    },
    edge: {
      desiredCapabilities: {
        platform: "Windows 10",
        browserName: "MicrosoftEdge",
        javascriptEnabled: true,
      },
    },
  },
});

if (platform === 'linux') {
  // pass no-sandbox for chrome headless
  config.test_settings.default.desiredCapabilities.chromeOptions = {
    args: ['--no-sandbox'],
  };
  // enable marionette for headless firefox
  config.test_settings.firefox.marionette = true;
}

module.exports = config;
