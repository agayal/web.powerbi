module.exports = {
  windows: {
    'webdriver.chrome.driver': 'node_modules/chromedriver/lib/chromedriver/chromedriver.exe',
    'webdriver.gecko.driver': 'node_modules/geckodriver/geckodriver.exe',
    'webdriver.edge.driver': 'node_modules/iedriver/lib/iedriver/IEDriverServer.exe',
  },
  mac: {
    'webdriver.chrome.driver': 'node_modules/chromedriver/lib/chromedriver/chromedriver',
    'webdriver.gecko.driver': 'node_modules/geckodriver/bin/geckodriver',
  },
  linux: {
    'webdriver.chrome.driver': 'node_modules/chromedriver/lib/chromedriver/chromedriver',
    'webdriver.gecko.driver': 'node_modules/geckodriver/bin/geckodriver',
  },
};
