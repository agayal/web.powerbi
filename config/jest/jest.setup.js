// import Enzyme, { shallow, render, mount } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { configure as rtlConfigure } from '@testing-library/dom';
import 'regenerator-runtime/runtime';


// Change getByTestId to use 'data-e2e' prop instead of 'data-testid';
rtlConfigure({testIdAttribute: 'data-e2e'});

// React 16 Enzyme adapter
Enzyme.configure({ adapter: new Adapter() });
