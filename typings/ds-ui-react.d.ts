declare module '@dealersocket/ds-ui-react' {
  import { MuiTheme } from 'material-ui/styles';
  export const theme: MuiTheme;
  export const withStyles: any;
  export const Tooltip: any;
}

declare module '@dealersocket/ds-ui-react/theme/ThemeProvider' {
  export const ThemeProvider: any;
}

declare module '@dealersocket/ds-ui-react/Typography' {
  export enum TypographyVariants {
    // MUI
    H1 = 'h1',
    H2 = 'h2',
    H3 = 'h3',
    H4 = 'h4',
    H5 = 'h5',
    H6 = 'h6',
    Subtitle1 = 'subtitle1',
    Subtitle2 = 'subtitle2',
    Body1 = 'body1',
    Body2 = 'body2',
    Caption = 'caption',
    Button = 'button',
    Overline = 'overline',
    SROnly = 'srOnly',
    Inherit = 'inherit',
    Banner = 'banner',
    FormError = 'formerror',
    ToolTip = 'tooltip',
    Warning = 'warning',
  }

  export const Typography: any;
}

declare module '@dealersocket/ds-ui-react/form/fields/FormNumberField' {
  export const FormNumberField: any;
}

declare module '@dealersocket/ds-ui-react/form/fields/FormTextField' {
  export const FormTextField: any;
}

declare module '@dealersocket/ds-ui-react/mui/Card' {
  export const Card: any;
}

declare module '@dealersocket/ds-ui-react/Avatar' {
  export const Avatar: any;
}

declare module '@dealersocket/ds-ui-react/fields/TextInputField' {
  export const TextInputField: any;
}
