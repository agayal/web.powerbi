export interface JssClasses {
  [key: string]: string;
}
