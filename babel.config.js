module.exports = (api) => {
  const BABEL_ENV = api.cache(() => process.env.BABEL_ENV);
  const NODE_ENV = api.cache(() => process.env.NODE_ENV);
  // eslint-disable-next-line no-console
  console.log('BABEL_ENV:', BABEL_ENV);
  // eslint-disable-next-line no-console
  console.log('NODE_ENV:', NODE_ENV);

  const presets = ['babel-preset-react-app'];
  let plugins = [];

  if (BABEL_ENV === 'development') {
    plugins = [
      'babel-plugin-typescript-to-proptypes',
      ['@babel/plugin-transform-typescript', { isTSX: true }],
    ];
  }

  return {
    presets,
    plugins,
  };
};
