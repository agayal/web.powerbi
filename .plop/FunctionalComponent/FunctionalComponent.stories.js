import React from 'react';
import { storiesOf } from '@storybook/react';
import {{properCase name}} from './{{dashCase name}}';

storiesOf('{{properCase name}}', module)
  .add('default', () => (
    <{{properCase name}} label="story" />
  ));
