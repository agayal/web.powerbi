import React from 'react';

function {{properCase name}}(props) {
  return (
    <div>{props.name}</div>
  );
}

{{properCase name}}.defaultProps = {
  name: '{{properCase name}}',
};

{{properCase name}}.propTypes = {
  name: React.PropTypes.string.isRequired,
};

export {{properCase name}};
