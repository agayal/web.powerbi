import React from 'react';
import { shallow } from 'enzyme';

import {{properCase name}} from './{{dashCase name}}';

describe('{{properCase name}}', () => {
  it('should return a function', () => {
    const actual = typeof {{properCase name}};
    const expected = 'function';
    expect(actual).toEqual(expected);
  });

  it('should render a container div', () => {
    const wrapper = shallow(<{{properCase name}} name="{{properCase name}}" />);
    expect(wrapper.find('div').length).toEqual(1);
  });

  it('should render the name', () => {
    const wrapper = shallow(<{{properCase name}} name="{{properCase name}}" />);
    expect(wrapper.text()).toContain('{{properCase name}}');
  });
});
