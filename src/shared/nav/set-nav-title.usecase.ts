import { createAction } from 'redux-actions';
import { reducer } from './nav.reducer';

const postfix = `/${reducer.sliceName}/app`;

const SET_NAV_TITLE: string = `SET_NAV_TITLE${postfix}`;
export const setNavTitleAction = createAction(SET_NAV_TITLE);

export const reduceHandlers = {
  [SET_NAV_TITLE]: (state: any, action: any) => {
    if (state.title !== action.payload) {
      return {
        ...state,
        title: action.payload,
      };
    }
    return state;
  },
};

reducer.addActionReducerMap(reduceHandlers);
