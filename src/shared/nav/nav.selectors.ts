import { createSelector } from 'reselect';
import { NavSliceType, reducer } from './nav.reducer';

const sliceSelector = (state: any): any => state[reducer.sliceName];

export const titleSelector = createSelector(
  sliceSelector,
  (slice: NavSliceType) => (slice ? slice.title : '')
);
