import { createAndMergeSliceReducer } from '@dealersocket/react-common';

const sliceName = 'navSlice';

export interface NavSliceType {
  title: string;
}

const initialState: NavSliceType = {
  title: 'Analytics 2.0',
};

export const reducer = createAndMergeSliceReducer(sliceName, initialState);
