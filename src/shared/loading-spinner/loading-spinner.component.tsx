import React from 'react';
import { DsLogoProgress } from '@dealersocket/ds-ui-react/DsLogoProgress';

export const LoadingSpinnerSize = {
  Small: 50,
  Large: 100,
};

interface LoadingSpinnerProps {
  size?: number;
}

export function LoadingSpinner({
  size,
}: LoadingSpinnerProps): React.ReactElement<LoadingSpinnerProps> {
  const sz = Number(size) || LoadingSpinnerSize.Large;
  return <DsLogoProgress style={{ width: sz, height: sz }} />;
}
