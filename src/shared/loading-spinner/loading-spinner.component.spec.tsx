import React from 'react';
import { mountWithProviders } from '@dealersocket/ds-ui-react/test-utils';
import { DsLogoProgress } from '@dealersocket/ds-ui-react/DsLogoProgress';
import { LoadingSpinner } from './loading-spinner.component';

describe('<LoadingSpinner>', () => {
  it('the LoadingSpinner should render properly', () => {
    const wrapper = mountWithProviders(<LoadingSpinner />);
    expect(wrapper.find(DsLogoProgress)).not.toBeNull();
  });
});
