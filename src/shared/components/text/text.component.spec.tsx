import React from 'react';

import { mountWithProviders } from '@dealersocket/ds-ui-react/test-utils';
import { TypographyVariants } from '@dealersocket/ds-ui-react/Typography';
import { Text } from './text.component';

describe('Text', () => {
  it('returns a function', () => {
    const actual = typeof Text;
    expect(actual).toEqual('function');
  });

  it('renders the children content', () => {
    const content = 'Pele > Maradona';
    const wrapper = mountWithProviders(
      <Text
        id="test"
        defaultMessage={content}
        variant={TypographyVariants.Body2}
      />
    );
    expect(wrapper.text()).toContain(content);
  });

  it('renders Body2', () => {
    const content = 'Pele > Maradona';
    const wrapper = mountWithProviders(
      <Text
        id="test"
        defaultMessage={content}
        variant={TypographyVariants.Body2}
      />
    );
    expect(wrapper.text()).toContain(content);
  });
});
