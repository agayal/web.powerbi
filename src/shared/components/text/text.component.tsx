import React from 'react';
import { FormattedMessage } from 'react-intl';
import {
  Typography,
  TypographyVariantType,
} from '@dealersocket/ds-ui-react/Typography';

export interface TextProps {
  id: string;
  defaultMessage: string;
  values?: any;
  variant: TypographyVariantType;
  className?: string;
  onClick?: () => void;
}

export function Text({
  id,
  defaultMessage,
  values,
  variant,
  ...others
}: TextProps): React.ReactElement<TextProps> {
  return (
    <Typography component="div" variant={variant} {...others}>
      <FormattedMessage
        id={id}
        defaultMessage={defaultMessage}
        values={values}
      />
    </Typography>
  );
}
