import { mountWithProviders } from '@dealersocket/ds-ui-react/test-utils';
import React from 'react';
import { setHistory } from '@dealersocket/react-common';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { SecureRouteContainer, testPort } from './secure-route.container';
import { SecureRoute } from './secure-route.component';
import { configureStore } from '../../../store';

describe('SecureRouteContainer', () => {
  const props: any = {
    importPath: 'area/console/view/console-page.container',
    path: 'area/console/view/console-page.container',
    componentName: 'ConsolePageContainer',
    permissionName: 'Analytics2Permission',
    darkFeatureName: 'Analytics2DarkFeature',
  };

  const store = configureStore({}, setHistory({}));

  it('should render a SecureRoute', () => {
    const component = mountWithProviders(
      <Provider store={store}>
        <StaticRouter location="/console" context={{}}>
          <SecureRouteContainer {...props} />
        </StaticRouter>
      </Provider>
    );
    expect(component.find(SecureRoute).length).toEqual(1);
  });

  it('should mapStateToProps with expected properties', () => {
    const mockState = {};
    const result = testPort.mapStateToProps(mockState);
    const expected = {};
    expect(result).toEqual(expected);
  });
});
