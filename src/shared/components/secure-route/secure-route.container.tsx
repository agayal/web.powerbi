import { connect } from 'react-redux';
import { SecureRoute } from './secure-route.component';
import { darkFeaturesSelector } from '../../state/dark-feature-permissions/dark-feature-permissions.selectors';
import { permissionsSelector } from '../../state/team-role-based-permissions/team-role-based-permissions.selectors';

const mapStateToProps = (state: any): any => {
  return {
    darkFeatureSlice: darkFeaturesSelector(state),
    permissionSlice: permissionsSelector(state),
  };
};

export const SecureRouteContainer = connect(
  mapStateToProps,
  {}
)(SecureRoute);

export const testPort = {
  mapStateToProps,
};
