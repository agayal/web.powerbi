import React from 'react';
import { Route } from 'react-router-dom';
import {
  TeamRoleBasedPermissionsSliceType,
  PermissionType,
} from '../../state/team-role-based-permissions/team-role-based-permissions.reducer';
import { UnauthorizedPage } from '../../../area/unauthorized/unauthorized.component';
import {
  DarkfeaturePermissionsSliceType,
  DarkFeaturePermissionsType,
} from '../../state/dark-feature-permissions/dark-feature-permissions.reducer';
import { ErrorPage } from '../../../area/error-page/error-page.component';
import { LoadingSpinner } from '../../loading-spinner';

export interface SecureRouteProps {
  darkFeatureSlice?: DarkfeaturePermissionsSliceType;
  permissionSlice?: TeamRoleBasedPermissionsSliceType;
  permissionName: string;
  darkFeatureName: string;
  path: string;
  component: any;
}

const hasAccess = (
  permissions: PermissionType[],
  permissionName: string,
  darkFeatures: DarkFeaturePermissionsType,
  darkFeatureName: string
): boolean => {
  const hasPermission =
    !permissionName ||
    (permissions.length > 0 &&
      permissions.filter((value) => value.key === permissionName)[0].view);
  const hasDarkFeature =
    !darkFeatureName || (darkFeatures as any)[darkFeatureName];

  return hasPermission && hasDarkFeature;
};

export const SecureRoute = ({
  permissionSlice,
  permissionName,
  darkFeatureSlice,
  darkFeatureName,
  path,
  component,
}: SecureRouteProps): React.ReactElement<SecureRouteProps> => {
  if (
    permissionSlice &&
    permissionSlice.permissions &&
    darkFeatureSlice &&
    darkFeatureSlice.permissions
  ) {
    return (
      <Route
        exact
        path={path}
        component={
          hasAccess(
            permissionSlice.permissions,
            permissionName,
            darkFeatureSlice.permissions,
            darkFeatureName
          )
            ? component
            : UnauthorizedPage
        }
      />
    );
  }

  if (
    (permissionSlice && permissionSlice.error) ||
    (darkFeatureSlice && darkFeatureSlice.error)
  ) {
    return <ErrorPage />;
  }

  return (
    <div style={{ margin: '200px auto' }}>
      <LoadingSpinner data-e2e="sd-spinner" />
    </div>
  );
};
