import React from 'react';
import { mountWithProviders } from '@dealersocket/ds-ui-react/test-utils';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import * as common from '@dealersocket/react-common';
import { ErrorPage } from '../../../area/error-page/error-page.component';
import { SecureRouteProps, SecureRoute } from './secure-route.component';
import { UnauthorizedPage } from '../../../area/unauthorized/unauthorized.component';
import {
  PermissionType,
  PermissionKeys,
} from '../../state/team-role-based-permissions/team-role-based-permissions.reducer';
import { configureStore } from '../../../store';
import { LoadingSpinner } from '../../loading-spinner';
import { ConsolePageContainer } from '../../../area/console/view/console-page.container';

describe('<SecureRoute>', () => {
  const store = configureStore({}, common.setHistory({}));

  const mockPermission: PermissionType = {
    key: PermissionKeys.Analytics2Permission,
    view: true,
    add: true,
    edit: true,
    title: 'Analytics2Permission',
    delete: true,
    restricted: false,
    enabledValues: [],
  };

  const mockInitialProps: SecureRouteProps = {
    path: '/console',
    component: ConsolePageContainer,
    permissionName: 'Analytics2Permission',
    darkFeatureName: 'Analytics2DarkFeature',
    darkFeatureSlice: {
      isLoading: false,
      error: '',
      permissions: {
        PowerBIDarkFeature: true,
      },
    },
    permissionSlice: {
      isLoading: false,
      error: '',
      permissions: [],
    },
  };

  it('should call lazy render if permissions are setup correctly', () => {
    const testprops = {
      ...mockInitialProps,
      permissionSlice: {
        isLoading: false,
        error: '',
        permissions: [mockPermission],
      },
      darkFeatureSlice: {
        isLoading: false,
        error: '',
        permissions: {
          PowerBIDarkFeature: true,
        },
      },
    };

    const wrapper = mountWithProviders(
      <Provider store={store}>
        <StaticRouter location="/console" context={{}}>
          <SecureRoute {...testprops} />
        </StaticRouter>
      </Provider>
    );
    expect(wrapper.find(ConsolePageContainer)).toHaveLength(1);
  });

  it("should render the loading spinner if permissions aren't available yet", () => {
    const testprops = {
      ...mockInitialProps,
      permissionSlice: {
        isLoading: true,
        error: '',
        permissions: undefined,
      },
    };

    const wrapper = mountWithProviders(
      <Provider store={store}>
        <StaticRouter location="/console" context={{}}>
          <SecureRoute {...testprops} />
        </StaticRouter>
      </Provider>
    );
    expect(wrapper.find(LoadingSpinner).length).toEqual(1);
  });

  it("should render the loading spinner if dark-features aren't available yet", () => {
    const testprops = {
      ...mockInitialProps,
      darkFeatureSlice: {
        isLoading: true,
        error: '',
        permissions: undefined,
      },
    };

    const wrapper = mountWithProviders(
      <Provider store={store}>
        <StaticRouter location="/console" context={{}}>
          <SecureRoute {...testprops} />
        </StaticRouter>
      </Provider>
    );
    expect(wrapper.find(LoadingSpinner).length).toEqual(1);
  });

  it('should display unauthorized page if permissions are not setup correctly', () => {
    const wrapper = mountWithProviders(
      <StaticRouter location="/console" context={{}}>
        <SecureRoute {...mockInitialProps} />
      </StaticRouter>
    );
    expect(wrapper.find(UnauthorizedPage).length).toEqual(1);
  });

  it('should display error page if error occurs when pulling permissions', () => {
    const testprops = {
      ...mockInitialProps,
      permissionSlice: {
        isLoading: false,
        error: `An error occured`,
        permissions: undefined,
      },
    };

    const wrapper = mountWithProviders(
      <StaticRouter location="/console" context={{}}>
        <SecureRoute {...testprops} />
      </StaticRouter>
    );
    expect(wrapper.find(ErrorPage).length).toEqual(1);
  });
});
