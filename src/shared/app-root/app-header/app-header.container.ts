import { connect } from 'react-redux';
import { pathnameSelector } from '../../../router.selectors';
import { titleSelector } from '../../nav/nav.selectors';
import { AppHeader } from './app-header.component';

function mapStateToProps(state: any): any {
  const pathname = pathnameSelector(state);
  const inMenu =
    ['/', '/hello', '/my', '/dallas', '/redux-demo'].indexOf(pathname) !== -1;
  return {
    pathname,
    inMenu,
    title: titleSelector(state),
  };
}

export const AppHeaderContainer = connect(mapStateToProps)(AppHeader);
