import React from 'react';
import { createStyles, withStyles, StyleRules } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import { FlexSpacer } from '@dealersocket/ds-ui-react/FlexSpacer';
import { ThemeType } from '@dealersocket/ds-ui-react/theme';
import { JssClasses } from '../../../../typings/styles/styles.type';

interface ExternalProps {
  pathname: string;
  inMenu: boolean;
  title: string;
}

interface InternalProps {
  classes: JssClasses;
}

type AppHeaderProps = ExternalProps & InternalProps;

interface AppHeaderStateType {
  anchorEl: any;
  open: boolean;
}

class AppHeaderComp extends React.Component<
  AppHeaderProps,
  AppHeaderStateType
> {
  openMenu = (event: any) => {
    // This prevents ghost click.
    event.preventDefault();
  };

  render(): any {
    const { classes } = this.props;

    return (
      <Toolbar classes={{ root: classes.toolbar }}>
        <h2 style={{ color: '#fff' }}>{this.props.title}</h2>
        <FlexSpacer />
      </Toolbar>
    );
  }
}

const styles = (theme: ThemeType): StyleRules =>
  createStyles({
    icon: {
      color: theme.palette.common.white,
    },
    toolbar: {
      height: theme.ds.spacing.desktopToolbarHeight,
      backgroundColor: theme.palette.primary.dark,
      paddingLeft: theme.ds.spacing.unit,
      paddingRight: theme.ds.spacing.unit,
    },
  });

export const AppHeader = withStyles(styles)(AppHeaderComp);
