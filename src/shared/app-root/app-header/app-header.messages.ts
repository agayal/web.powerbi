/*
 * Header Messages
 *
 * This contains all the text for the Header component.
 */
import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  home: {
    id: 'web.app.template.header.home',
    defaultMessage: 'Home Not Found',
  },
});
