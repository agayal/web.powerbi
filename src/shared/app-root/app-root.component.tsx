/**
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */
// import Helmet from 'react-helmet';
import React from 'react';
import ReduxToastr from 'react-redux-toastr';
import { createStyles, withStyles, StyleRules } from '@material-ui/core/styles';

import { AppHeaderContainer } from './app-header/app-header.container';
import { Routes } from '../../routes';

interface AppRootCompProps {
  classes: any;
}

const AppRootComp = ({
  classes,
}: AppRootCompProps): React.ReactElement<AppRootCompProps> => (
  <div className={classes.root}>
    <AppHeaderContainer pathname="" inMenu title="" />
    <Routes />
    <ReduxToastr timeOut={2000} transitionIn="fadeIn" transitionOut="fadeOut" />
  </div>
);

const styles: StyleRules = createStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100%',
  },
});

export const AppRoot = withStyles(styles)(AppRootComp);
