/* eslint-disable func-names */
import { axiosApi, getAppSettings } from '@dealersocket/react-common';

type AxiosCall = (uri: string, data?: any, headers?: any) => Promise<any>;

function apiCall(method: string): AxiosCall {
  return async function(uri: string, data?: any, headers?: any): Promise<any> {
    return axiosApi(getAppSettings().globalApiUrl + uri, {
      method,
      data,
      headers,
    });
  };
}

export const httpGet: AxiosCall = apiCall('get');
