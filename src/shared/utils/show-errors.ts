import { toastr } from 'react-redux-toastr';

export function showReason(reason: any): void {
  showError(reason);
}

export function showError(error: any): void {
  let logTitle = 'Error';
  let msgObj = error;
  if (error.response && error.response.data) {
    msgObj = error.response.data;
    logTitle = error.toString();
  }
  const msg = msgObj.message ? msgObj.message : msgObj.toString();
  toastr.error(msg, '');
  // eslint-disable-next-line no-console
  console.error(logTitle, '-', msg);
}

export function showErrors(promise: any): Promise<any> {
  return promise.catch((reason: any) => {
    showError(reason);
    // Return new rejected promise
    return Promise.reject(reason);
  });
}
