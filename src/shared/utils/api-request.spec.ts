import * as common from '@dealersocket/react-common';

import { httpGet } from './api-request';

describe('api-request', () => {
  const mockAppSettings = { globalApiUrl: 'url' };

  const getAppSettingsSpy = jest
    .spyOn(common, 'getAppSettings')
    .mockImplementation(() => mockAppSettings);

  beforeEach(() => {
    getAppSettingsSpy.mockClear();
  });

  it('should do a get', async () => {
    const message = 'Hello, world!';
    const axiosCallSpy = jest
      .spyOn(common, 'axiosApi')
      .mockImplementation(() => message);

    const response = await httpGet('/stuff');

    expect(axiosCallSpy).toHaveBeenCalled();
    expect(response).toEqual(message);
  });
});
