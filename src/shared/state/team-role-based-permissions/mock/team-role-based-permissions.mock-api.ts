import { axiosResult, getAppSettings } from '@dealersocket/react-common';
import teamRoleBasedPermissions from './team-role-based-permissions.json';

export function mockApi(axiosMock: any): any {
  axiosMock
    .onPost(`${getAppSettings().globalUrl}/api/permissions`)
    .reply(() => {
      return axiosResult(teamRoleBasedPermissions);
    });
}
