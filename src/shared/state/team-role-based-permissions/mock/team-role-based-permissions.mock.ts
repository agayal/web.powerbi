import { PermissionKeys } from '../team-role-based-permissions.reducer';

export const mockPermissions = [
  {
    title: 'Analytics 2.0',
    key: PermissionKeys.Analytics2Permission,
    add: true,
    edit: true,
    delete: true,
    view: true,
    restricted: false,
    enabledValues: [],
  },
];
