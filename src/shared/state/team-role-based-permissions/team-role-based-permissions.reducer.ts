import { createAndMergeSliceReducer } from '@dealersocket/react-common';

const sliceName = 'teamRoleBasedPermissions';

export interface PermissionType {
  key: PermissionKeys;
  title: string;
  view: boolean;
  add: boolean;
  edit: boolean;
  delete: boolean;
  restricted: boolean;
  enabledValues: string[];
}

export enum PermissionKeys {
  Analytics2Permission = 'Analytics2Permission',
}

export interface PermissionJsonType {
  Key: PermissionKeys;
  Title: string;
  View: boolean;
  Add: boolean;
  Edit: boolean;
  Delete: boolean;
  Restricted: boolean;
  EnabledValues: string[];
}

export interface TeamRoleBasedPermissionsSliceType {
  isLoading: boolean;
  error: string | undefined;
  permissions: PermissionType[] | undefined;
}

const initialState: TeamRoleBasedPermissionsSliceType = {
  isLoading: false,
  error: undefined,
  permissions: undefined,
};

export const teamRoleBasedPermissionsReducer = createAndMergeSliceReducer(
  sliceName,
  initialState
);

export enum EnabledValues {}
