import {
  PermissionType,
  PermissionKeys,
} from './team-role-based-permissions.reducer';
import { permissionsSelector } from './team-role-based-permissions.selectors';

export const getMultiplePermissionsByKey = (
  state: any,
  ...requestedPermissions: PermissionKeys[]
): Map<PermissionKeys, PermissionType> => {
  const result: Map<PermissionKeys, PermissionType> = new Map();

  const { permissions } = permissionsSelector(state);

  const foundPermissions = permissions
    ? permissions.filter((permission: PermissionType) =>
        requestedPermissions.includes(permission.key)
      )
    : [];

  foundPermissions.forEach((permission: PermissionType) => {
    result.set(permission.key, permission);
  });

  return result;
};

export const getSinglePermissionByKey = (
  permissions: PermissionType[] | undefined,
  key: string
): PermissionType | undefined => {
  const foundPermission = permissions
    ? permissions.find((permission: PermissionType) => permission.key === key)
    : undefined;
  return foundPermission;
};
