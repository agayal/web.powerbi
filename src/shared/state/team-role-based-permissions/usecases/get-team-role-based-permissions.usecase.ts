import { createAction, mergeSaga } from '@dealersocket/redux-utils';
import { SagaIterator } from 'redux-saga';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import {
  APP_INIT,
  DEALERSHIP_CHANGED,
  getAppSettings,
  axiosApi,
  dealershipIdSelector,
} from '@dealersocket/react-common';
import { LOCATION_CHANGE } from 'connected-react-router';
import {
  TeamRoleBasedPermissionsSliceType,
  teamRoleBasedPermissionsReducer,
  PermissionJsonType,
  PermissionType,
} from '../team-role-based-permissions.reducer';

const suffix = `/${teamRoleBasedPermissionsReducer.sliceName}/app`;

const LOAD_TEAMROLEBASED_PERMISSIONS = `LOAD_TEAMROLEBASED_PERMISSIONS${suffix}`;
const loadTeamRoleBasedPermissionsAction = createAction(
  LOAD_TEAMROLEBASED_PERMISSIONS
);
const LOAD_TEAMROLEBASED_PERMISSIONS_ERROR = `LOAD_TEAMROLEBASED_PERMISSIONS_ERROR${suffix}`;
const loadTeamRoleBasedPermissionsErrorAction = createAction<string>(
  LOAD_TEAMROLEBASED_PERMISSIONS_ERROR
);
const LOAD_TEAMROLEBASED_PERMISSIONS_SUCCESS = `LOAD_TEAMROLEBASED_PERMISSIONS_SUCCESS${suffix}`;
const loadTeamRoleBasedPermissionsSuccessAction = createAction<
  PermissionType[]
>(LOAD_TEAMROLEBASED_PERMISSIONS_SUCCESS);

function* loadTeamRoleBasedPermissionsWatcher(): SagaIterator {
  yield takeLatest(
    [APP_INIT, DEALERSHIP_CHANGED, LOCATION_CHANGE],
    loadTeamRoleBasedPermissionsWorker
  );
}
mergeSaga(loadTeamRoleBasedPermissionsWatcher);

function* loadTeamRoleBasedPermissionsWorker(): SagaIterator {
  try {
    const siteId: number = yield select(dealershipIdSelector);

    const response: PermissionJsonType[] = yield call(
      getApiTeamRoleBasedPermissions,
      siteId
    );
    const permissions: PermissionType[] = yield call(
      deserializePermissions,
      response
    );
    yield put(loadTeamRoleBasedPermissionsSuccessAction(permissions));
  } catch (e) {
    yield put(loadTeamRoleBasedPermissionsErrorAction(e.message));
  }
}

const getApiTeamRoleBasedPermissions = (
  siteId: number
): PermissionJsonType[] => {
  return axiosApi(`${getAppSettings().globalUrl}/api/permissions`, {
    method: 'post',
    data: {},
    headers: {
      dealershipId: siteId,
    },
  });
};

const deserializePermissions = (
  jsonArray: PermissionJsonType[]
): PermissionType[] => {
  return jsonArray.map((json: PermissionJsonType) => {
    return {
      title: json.Title,
      key: json.Key,
      view: json.View,
      add: json.Add,
      edit: json.Edit,
      delete: json.Delete,
      restricted: json.Restricted,
      enabledValues: json.EnabledValues,
    };
  });
};

const reduceHandlers = {
  [LOAD_TEAMROLEBASED_PERMISSIONS]: (
    slice: TeamRoleBasedPermissionsSliceType
  ): TeamRoleBasedPermissionsSliceType => ({
    ...slice,
    isLoading: true,
    error: undefined,
  }),
  [LOAD_TEAMROLEBASED_PERMISSIONS_SUCCESS]: (
    slice: TeamRoleBasedPermissionsSliceType,
    action: any
  ): TeamRoleBasedPermissionsSliceType => ({
    ...slice,
    isLoading: false,
    permissions: action.payload,
  }),
  [LOAD_TEAMROLEBASED_PERMISSIONS_ERROR]: (
    slice: TeamRoleBasedPermissionsSliceType,
    action: any
  ): TeamRoleBasedPermissionsSliceType => ({
    ...slice,
    isLoading: false,
    error: action.payload,
  }),
};
teamRoleBasedPermissionsReducer.addHandlers(reduceHandlers);

export const testPort = {
  LOAD_TEAMROLEBASED_PERMISSIONS,
  LOAD_TEAMROLEBASED_PERMISSIONS_SUCCESS,
  LOAD_TEAMROLEBASED_PERMISSIONS_ERROR,
  loadTeamRoleBasedPermissionsAction,
  loadTeamRoleBasedPermissionsErrorAction,
  loadTeamRoleBasedPermissionsSuccessAction,
  loadTeamRoleBasedPermissionsWatcher,
  loadTeamRoleBasedPermissionsWorker,
  getApiTeamRoleBasedPermissions,
  deserializePermissions,
  reduceHandlers,
};
