import { call, put, select, takeLatest } from 'redux-saga/effects';
import {
  cloneableGenerator,
  SagaIteratorClone,
} from '@redux-saga/testing-utils';
import * as common from '@dealersocket/react-common';
import { LOCATION_CHANGE } from 'connected-react-router';
import { testPort } from './get-team-role-based-permissions.usecase';
import {
  TeamRoleBasedPermissionsSliceType,
  PermissionJsonType,
  PermissionType,
  PermissionKeys,
} from '../team-role-based-permissions.reducer';

const {
  LOAD_TEAMROLEBASED_PERMISSIONS,
  LOAD_TEAMROLEBASED_PERMISSIONS_SUCCESS,
  LOAD_TEAMROLEBASED_PERMISSIONS_ERROR,
  loadTeamRoleBasedPermissionsAction,
  loadTeamRoleBasedPermissionsErrorAction,
  loadTeamRoleBasedPermissionsSuccessAction,
  loadTeamRoleBasedPermissionsWatcher,
  loadTeamRoleBasedPermissionsWorker,
  getApiTeamRoleBasedPermissions,
  deserializePermissions,
  reduceHandlers,
} = testPort;

describe('Get Team Roule Based Permissions Usecase', () => {
  const mockSiteId: number = 555;
  const mockPermission: PermissionType = {
    key: PermissionKeys.socketTalk,
    view: true,
    add: true,
    edit: true,
    title: 'SocketTalk',
    delete: true,
    restricted: false,
    enabledValues: [],
  };
  const mockPermissionJSON: PermissionJsonType = {
    Key: PermissionKeys.socketTalk,
    View: true,
    Add: true,
    Edit: true,
    Title: 'SocketTalk',
    Delete: true,
    Restricted: false,
    EnabledValues: [],
  };

  it(`should watch for ${common.APP_INIT} and ${
    common.DEALERSHIP_CHANGED
  } actions`, () => {
    const generator: SagaIteratorClone = cloneableGenerator(
      loadTeamRoleBasedPermissionsWatcher
    )();
    expect(generator.next().value).toEqual(
      takeLatest(
        [common.APP_INIT, common.DEALERSHIP_CHANGED, LOCATION_CHANGE],
        loadTeamRoleBasedPermissionsWorker
      )
    );
  });

  it('handles LOAD_TEAMROLEBASED_PERMISSIONS', () => {
    const initialState: TeamRoleBasedPermissionsSliceType = {
      isLoading: false,
      error: undefined,
      permissions: [],
    };
    const expectedState: TeamRoleBasedPermissionsSliceType = {
      isLoading: true,
      error: undefined,
      permissions: [],
    };

    const state = reduceHandlers[LOAD_TEAMROLEBASED_PERMISSIONS](
      initialState,
      loadTeamRoleBasedPermissionsAction()
    );
    expect(state).toEqual(expectedState);
  });

  it('handles LOAD_TEAMROLEBASED_PERMISSIONS_ERROR', () => {
    const errorMsg = 'I am error!';
    const initialState: TeamRoleBasedPermissionsSliceType = {
      isLoading: true,
      error: undefined,
      permissions: [],
    };
    const expectedState: TeamRoleBasedPermissionsSliceType = {
      isLoading: false,
      error: errorMsg,
      permissions: [],
    };
    const state = reduceHandlers[LOAD_TEAMROLEBASED_PERMISSIONS_ERROR](
      initialState,
      loadTeamRoleBasedPermissionsErrorAction(errorMsg)
    );
    expect(state).toEqual(expectedState);
  });

  it('handles LOAD_TEAMROLEBASED_PERMISSIONS_SUCCESS', () => {
    const initialState: TeamRoleBasedPermissionsSliceType = {
      isLoading: true,
      error: undefined,
      permissions: [],
    };
    const expectedState: TeamRoleBasedPermissionsSliceType = {
      isLoading: false,
      error: undefined,
      permissions: [],
    };
    const state = reduceHandlers[LOAD_TEAMROLEBASED_PERMISSIONS_SUCCESS](
      initialState,
      loadTeamRoleBasedPermissionsSuccessAction([])
    );
    expect(state).toEqual(expectedState);
  });

  it('should call the API endpoint', () => {
    const axiosCall: jest.SpyInstance = jest
      .spyOn(common, 'axiosApi')
      .mockImplementation(() => 'Called');
    const getAppSettings: jest.SpyInstance = jest
      .spyOn(common, 'getAppSettings')
      .mockImplementation(() => ({ globalApiUrl: 'url' }));
    testPort.getApiTeamRoleBasedPermissions(1);
    expect(axiosCall).toHaveBeenCalled();
    expect(getAppSettings).toHaveBeenCalled();
  });

  it('should correctly deserialize the permissions', () => {
    const mockPermissionJSONArray: PermissionJsonType[] = [mockPermissionJSON];
    const expected: PermissionType[] = [mockPermission];
    const result: PermissionType[] = testPort.deserializePermissions(
      mockPermissionJSONArray
    );
    expect(expected).toEqual(result);
  });

  it('should handle success', () => {
    const generator: SagaIteratorClone = cloneableGenerator(
      loadTeamRoleBasedPermissionsWorker
    )();
    const mockResponseJSON: PermissionJsonType[] = [mockPermissionJSON];
    const mockResponse: PermissionType[] = [mockPermission];
    expect(generator.next().value).toEqual(select(common.dealershipIdSelector));
    expect(generator.next(mockSiteId).value).toEqual(
      call(getApiTeamRoleBasedPermissions, mockSiteId)
    );
    expect(generator.next(mockResponseJSON).value).toEqual(
      call(deserializePermissions, mockResponseJSON)
    );
    expect(generator.next(mockResponse).value).toEqual(
      put(loadTeamRoleBasedPermissionsSuccessAction(mockResponse))
    );
    expect(generator.next().done).toBe(true);
  });

  it('should handle an exception', () => {
    const generator: SagaIteratorClone = cloneableGenerator(
      loadTeamRoleBasedPermissionsWorker
    )();
    const errorMsg: string = 'GAAAA! BLAAAARG!';
    const error: Error = new Error(errorMsg);
    expect(generator.next().value).toEqual(select(common.dealershipIdSelector));
    expect(generator.next(mockSiteId).value).toEqual(
      call(getApiTeamRoleBasedPermissions, mockSiteId)
    );
    expect(generator.throw && generator.throw(error).value).toEqual(
      put(loadTeamRoleBasedPermissionsErrorAction(errorMsg))
    );
    expect(generator.next().done).toBe(true);
  });
});
