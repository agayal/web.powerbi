import { createSelector } from 'reselect';
import {
  TeamRoleBasedPermissionsSliceType,
  teamRoleBasedPermissionsReducer,
} from './team-role-based-permissions.reducer';

export const permissionsSelector = (
  state: any
): TeamRoleBasedPermissionsSliceType =>
  state[teamRoleBasedPermissionsReducer.sliceName];

export const teamRoleBasedPermissionsSelector = createSelector(
  permissionsSelector,
  (permissionSlice: TeamRoleBasedPermissionsSliceType) =>
    permissionSlice.permissions
);
