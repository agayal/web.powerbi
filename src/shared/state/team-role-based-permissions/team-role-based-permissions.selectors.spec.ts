import {
  teamRoleBasedPermissionsReducer,
  PermissionType,
  PermissionKeys,
} from './team-role-based-permissions.reducer';
import {
  permissionsSelector,
  teamRoleBasedPermissionsSelector,
} from './team-role-based-permissions.selectors';

describe('Team Role Based Permissions selector', () => {
  const mockPermission: PermissionType = {
    key: PermissionKeys.Analytics2Permission,
    view: true,
    add: true,
    edit: true,
    title: 'Analytics 2.0',
    delete: true,
    restricted: false,
    enabledValues: [],
  };

  it('should handle empty slice', () => {
    const mockStore = {
      [teamRoleBasedPermissionsReducer.sliceName]: {},
    };
    const result = permissionsSelector(mockStore);
    expect(result).toEqual({});
  });

  it('should handle populated slice', () => {
    const mockStore = {
      [teamRoleBasedPermissionsReducer.sliceName]: {
        permissions: [mockPermission],
      },
    };
    const result = permissionsSelector(mockStore);
    expect(result).toEqual({ permissions: [mockPermission] });
  });

  it('should handle populated slice for teamRoleBasedPermissionsSelector', () => {
    const mockStore = {
      [teamRoleBasedPermissionsReducer.sliceName]: {
        permissions: mockPermission,
      },
    };
    const result = teamRoleBasedPermissionsSelector(mockStore);
    expect(result).toEqual(mockPermission);
  });
});
