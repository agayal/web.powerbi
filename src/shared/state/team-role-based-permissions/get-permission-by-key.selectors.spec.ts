// eslint-disable-next-line @dealersocket/dealersocket/file-names
import {
  teamRoleBasedPermissionsReducer,
  PermissionType,
  PermissionKeys,
} from './team-role-based-permissions.reducer';
import {
  getMultiplePermissionsByKey,
  getSinglePermissionByKey,
} from './get-permission-by-key.selectors';

describe('get multiple permissions by title selector', () => {
  const mockPermission: PermissionType = {
    title: 'SalesSold',
    key: PermissionKeys.salesSold,
    view: true,
    add: true,
    edit: true,
    delete: true,
    restricted: false,
    enabledValues: [],
  };

  it('should handle empty slice', () => {
    const mockStore = { [teamRoleBasedPermissionsReducer.sliceName]: {} };
    const result = getMultiplePermissionsByKey(mockStore);
    expect(result).toEqual(new Map());
  });

  it('should handle empty permissions', () => {
    const mockStore = {
      [teamRoleBasedPermissionsReducer.sliceName]: {
        permissions: [],
      },
    };
    const result = getMultiplePermissionsByKey(mockStore);
    expect(result).toMatchObject(new Map());
  });

  it('should handle populated slice', () => {
    const mockStore = {
      [teamRoleBasedPermissionsReducer.sliceName]: {
        permissions: [mockPermission],
      },
    };
    const result = getMultiplePermissionsByKey(mockStore, mockPermission.key);
    const permission = result.get(PermissionKeys.salesSold);
    const salesSoldView = permission && permission.view;
    expect(salesSoldView).toEqual(true);
  });
});

describe('get single permission by key selector', () => {
  const mockPermission: PermissionType = {
    title: 'SalesSold',
    key: PermissionKeys.salesSold,
    view: true,
    add: true,
    edit: true,
    delete: true,
    restricted: false,
    enabledValues: [],
  };

  it('should handle empty permissions', () => {
    const result = getSinglePermissionByKey([], PermissionKeys.salesSold);
    expect(result).toBeUndefined();
  });

  it('should handle undefined permissions', () => {
    const result = getSinglePermissionByKey(
      undefined,
      PermissionKeys.salesSold
    );
    expect(result).toBeUndefined();
  });

  it('should handle populated permissions array', () => {
    const result = getSinglePermissionByKey(
      [mockPermission],
      mockPermission.key
    );
    expect(result).toEqual(mockPermission);
  });
});
