import { createAndMergeSliceReducer } from '@dealersocket/react-common';

const sliceName = 'darkfeaturePermissions';

export interface DarkFeaturePermissionsType {
  PowerBIDarkFeature: boolean;
}

export interface DarkfeaturePermissionsSliceType {
  isLoading: boolean;
  error: string | undefined;
  permissions: DarkFeaturePermissionsType | undefined;
}

const initialState: DarkfeaturePermissionsSliceType = {
  isLoading: false,
  error: undefined,
  permissions: undefined,
};

export const darkfeaturePermissionsReducer = createAndMergeSliceReducer(
  sliceName,
  initialState
);
