import {
  darkfeaturePermissionsReducer,
  DarkfeaturePermissionsSliceType,
} from './dark-feature-permissions.reducer';

export const darkFeaturesSelector = (
  state: any
): DarkfeaturePermissionsSliceType =>
  state[darkfeaturePermissionsReducer.sliceName];
