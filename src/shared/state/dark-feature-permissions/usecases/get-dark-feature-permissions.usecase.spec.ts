import { SagaIterator } from 'redux-saga';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { cloneableGenerator } from '@redux-saga/testing-utils';
import * as common from '@dealersocket/react-common';
import { LOCATION_CHANGE } from 'connected-react-router';
import { testPort } from './get-dark-feature-permissions.usecase';
import {
  DarkfeaturePermissionsSliceType,
  DarkFeaturePermissionsType,
} from '../dark-feature-permissions.reducer';

const {
  LOAD_DARKFEATURE_PERMISSIONS,
  LOAD_DARKFEATURE_PERMISSIONS_SUCCESS,
  LOAD_DARKFEATURE_PERMISSIONS_ERROR,
  loadDarkFeaturePermissionsAction,
  loadDarkFeaturePermissionsErrorAction,
  loadDarkFeaturePermissionsSuccessAction,
  loadDarkFeaturePermissionsWatcher,
  loadDarkFeaturePermissionsWorker,
  getApiDarkfeaturePermissions,
  reduceHandlers,
} = testPort;

describe('Get dark feature permissions usecase', () => {
  const mockSiteId: number = 555;

  const mockPermissionsFalse: DarkFeaturePermissionsType = {
    PowerBIDarkFeature: false,
  };

  const mockPermissionsTrue: DarkFeaturePermissionsType = {
    PowerBIDarkFeature: true,
  };

  it(`should watch for ${common.APP_INIT} and ${common.DEALERSHIP_CHANGED} actions`, () => {
    const generator: SagaIterator = cloneableGenerator(
      loadDarkFeaturePermissionsWatcher
    )();
    expect(generator.next().value).toEqual(
      takeLatest(
        [common.APP_INIT, common.DEALERSHIP_CHANGED, LOCATION_CHANGE],
        loadDarkFeaturePermissionsWorker
      )
    );
  });

  it('handles LOAD_DARKFEATURE_PERMISSIONS', () => {
    const initialState: DarkfeaturePermissionsSliceType = {
      isLoading: false,
      error: undefined,
      permissions: mockPermissionsFalse,
    };
    const expectedState: DarkfeaturePermissionsSliceType = {
      isLoading: true,
      error: undefined,
      permissions: mockPermissionsFalse,
    };

    const state = reduceHandlers[LOAD_DARKFEATURE_PERMISSIONS](
      initialState,
      loadDarkFeaturePermissionsAction()
    );
    expect(state).toEqual(expectedState);
  });

  it('handles LOAD_DARKFEATURE_PERMISSIONS_ERROR', () => {
    const errorMsg = 'Gaaaa! BLAAAARG!!!';
    const initialState: DarkfeaturePermissionsSliceType = {
      isLoading: true,
      error: undefined,
      permissions: mockPermissionsFalse,
    };
    const expectedState: DarkfeaturePermissionsSliceType = {
      isLoading: false,
      error: errorMsg,
      permissions: mockPermissionsFalse,
    };
    const state = reduceHandlers[LOAD_DARKFEATURE_PERMISSIONS_ERROR](
      initialState,
      loadDarkFeaturePermissionsErrorAction(errorMsg)
    );
    expect(state).toEqual(expectedState);
  });

  it('handles LOAD_DARKFEATURE_PERMISSIONS_SUCCESS', () => {
    const initialState: DarkfeaturePermissionsSliceType = {
      isLoading: true,
      error: undefined,
      permissions: mockPermissionsFalse,
    };
    const expectedState: DarkfeaturePermissionsSliceType = {
      isLoading: false,
      error: undefined,
      permissions: mockPermissionsTrue,
    };
    const state = reduceHandlers[LOAD_DARKFEATURE_PERMISSIONS_SUCCESS](
      initialState,
      loadDarkFeaturePermissionsSuccessAction(mockPermissionsTrue)
    );
    expect(state).toEqual(expectedState);
  });

  // it('should call the API endpoint', () => {
  //   const axiosCall: jest.SpyInstance = jest
  //     .spyOn(common, 'axiosApi')
  //     .mockImplementation(() => 'Called');
  //   const getAppSettings: jest.SpyInstance = jest
  //     .spyOn(common, 'getAppSettings')
  //     .mockImplementation(() => ({ globalApiUrl: 'url' }));
  //   testPort.getApiDarkfeaturePermissions(mockSiteId);
  //   expect(axiosCall).toHaveBeenCalled();
  //   expect(getAppSettings).toHaveBeenCalled();
  // });

  it('should handle success', () => {
    const generator = cloneableGenerator(loadDarkFeaturePermissionsWorker)();
    const mockResponse = {
      DarkFeaturePermissions: mockPermissionsTrue,
    };
    expect(generator.next().value).toEqual(select(common.dealershipIdSelector));
    expect(generator.next(mockSiteId).value).toEqual(
      call(getApiDarkfeaturePermissions, mockSiteId)
    );
    expect(generator.next(mockResponse).value).toEqual(
      put(
        loadDarkFeaturePermissionsSuccessAction(
          mockResponse.DarkFeaturePermissions
        )
      )
    );
    expect(generator.next().done).toBeTruthy();
  });

  it('should handle an exception', () => {
    const generator = cloneableGenerator(loadDarkFeaturePermissionsWorker)();
    const errorMsg = 'GAAAA! BLAAAARG!';
    const error = new Error(errorMsg);
    expect(generator.next().value).toEqual(select(common.dealershipIdSelector));
    expect(generator.next(mockSiteId).value).toEqual(
      call(getApiDarkfeaturePermissions, mockSiteId)
    );
    expect(generator.throw && generator.throw(error).value).toEqual(
      put(loadDarkFeaturePermissionsErrorAction(errorMsg))
    );
    expect(generator.next().done).toBeTruthy();
  });
});
