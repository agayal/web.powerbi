import { SagaIterator } from 'redux-saga';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import {
  APP_INIT,
  DEALERSHIP_CHANGED,
  mergeSaga,
  createAction,
  dealershipIdSelector,
  axiosApi,
  getAppSettings,
} from '@dealersocket/react-common';
import { LOCATION_CHANGE } from 'connected-react-router';
import {
  darkfeaturePermissionsReducer,
  DarkFeaturePermissionsType,
  DarkfeaturePermissionsSliceType,
} from '../dark-feature-permissions.reducer';

const suffix = `/${darkfeaturePermissionsReducer.sliceName}/app`;

const LOAD_DARKFEATURE_PERMISSIONS = `LOAD_DARKFEATURE_PERMISSIONS${suffix}`;
const loadDarkFeaturePermissionsAction = createAction(
  LOAD_DARKFEATURE_PERMISSIONS
);
const LOAD_DARKFEATURE_PERMISSIONS_ERROR = `LOAD_DARKFEATURE_PERMISSIONS_ERROR${suffix}`;
const loadDarkFeaturePermissionsErrorAction = createAction<string>(
  LOAD_DARKFEATURE_PERMISSIONS_ERROR
);
const LOAD_DARKFEATURE_PERMISSIONS_SUCCESS = `LOAD_DARKFEATURE_PERMISSIONS_SUCCESS${suffix}`;
const loadDarkFeaturePermissionsSuccessAction = createAction<
  DarkFeaturePermissionsType
>(LOAD_DARKFEATURE_PERMISSIONS_SUCCESS);

function* loadDarkFeaturePermissionsWatcher(): SagaIterator {
  yield takeLatest(
    [APP_INIT, DEALERSHIP_CHANGED, LOCATION_CHANGE],
    loadDarkFeaturePermissionsWorker
  );
}
mergeSaga(loadDarkFeaturePermissionsWatcher);

function* loadDarkFeaturePermissionsWorker(): SagaIterator {
  try {
    const siteId: number = yield select(dealershipIdSelector);
    const response: {
      SiteId: number;
      DarkFeaturePermissions: DarkFeaturePermissionsType;
    } = yield call(getApiDarkfeaturePermissions, siteId);

    yield put(
      loadDarkFeaturePermissionsSuccessAction(<DarkFeaturePermissionsType>(
        response.DarkFeaturePermissions
      ))
    );
  } catch (e) {
    yield put(loadDarkFeaturePermissionsErrorAction(e.message));
  }
}

const getApiDarkfeaturePermissions = (
  siteId: number
): DarkFeaturePermissionsType => {
  return axiosApi(
    `${
      getAppSettings().globalApiUrl
    }/dealerships/${siteId}/darkFeaturePermissions`,
    {}
  );
};

const reduceHandlers = {
  [LOAD_DARKFEATURE_PERMISSIONS]: (
    slice: DarkfeaturePermissionsSliceType
  ): DarkfeaturePermissionsSliceType => ({
    ...slice,
    isLoading: true,
    error: undefined,
  }),
  [LOAD_DARKFEATURE_PERMISSIONS_SUCCESS]: (
    slice: DarkfeaturePermissionsSliceType,
    action: any
  ): DarkfeaturePermissionsSliceType => ({
    ...slice,
    isLoading: false,
    permissions: action.payload,
  }),
  [LOAD_DARKFEATURE_PERMISSIONS_ERROR]: (
    slice: DarkfeaturePermissionsSliceType,
    action: any
  ): DarkfeaturePermissionsSliceType => ({
    ...slice,
    isLoading: false,
    error: action.payload,
  }),
};
darkfeaturePermissionsReducer.addHandlers(reduceHandlers);

export const testPort = {
  LOAD_DARKFEATURE_PERMISSIONS,
  LOAD_DARKFEATURE_PERMISSIONS_SUCCESS,
  LOAD_DARKFEATURE_PERMISSIONS_ERROR,
  loadDarkFeaturePermissionsAction,
  loadDarkFeaturePermissionsErrorAction,
  loadDarkFeaturePermissionsSuccessAction,
  loadDarkFeaturePermissionsWatcher,
  loadDarkFeaturePermissionsWorker,
  getApiDarkfeaturePermissions,
  reduceHandlers,
};
