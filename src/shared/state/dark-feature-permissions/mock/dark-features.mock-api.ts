import { axiosResult, getAppSettings } from '@dealersocket/react-common';
import darkFeatures from './dark-features.json';

export function mockApi(axiosMock: any): any {
  axiosMock
    .onGet(
      `${
        getAppSettings().globalApiUrl
      }/dealerships/undefined/darkFeaturePermissions`
    )
    .reply(() => {
      return axiosResult(darkFeatures);
    });
}
