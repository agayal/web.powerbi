import { darkFeaturesSelector } from './dark-feature-permissions.selectors';
import { darkfeaturePermissionsReducer } from './dark-feature-permissions.reducer';
import { mockDarkFeatures } from '../../mocks/mock-dark-features.mock';

describe('Dark Features Selector', () => {
  const mockDarkFeaturePermissions = mockDarkFeatures;

  it('should handle empty slice', () => {
    const mockStore = {
      [darkfeaturePermissionsReducer.sliceName]: {},
    };
    const result = darkFeaturesSelector(mockStore);
    expect(result).toEqual({});
  });

  it('should handle populated slice', () => {
    const mockStore = {
      [darkfeaturePermissionsReducer.sliceName]: {
        permissions: mockDarkFeaturePermissions,
      },
    };
    const result = darkFeaturesSelector(mockStore);
    expect(result).toEqual({ permissions: mockDarkFeaturePermissions });
  });
});
