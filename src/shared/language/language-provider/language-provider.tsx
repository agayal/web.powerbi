/*
 *
 * LanguageProvider
 *
 * this component connects the redux state language locale to the
 * IntlProvider component and i18n messages (loaded from `app/translations`)
 */

import React from 'react';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { localeSelector } from '../language.selectors';

interface LanguageProviderProps {
  locale: string;
  messages: any;
  children: any;
}

const LanguageProviderComp = (
  props: LanguageProviderProps
): React.ReactElement<LanguageProviderProps> => {
  const { locale, messages, children } = props;
  return (
    <IntlProvider locale={locale} key={locale} messages={messages[locale]}>
      {React.Children.only(children)}
    </IntlProvider>
  );
};

function mapStateToProps(state: any): any {
  return {
    locale: localeSelector(state),
  };
}

export const LanguageProvider: any = connect(mapStateToProps)(
  LanguageProviderComp
);
