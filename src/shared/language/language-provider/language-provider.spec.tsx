import { setHistory } from '@dealersocket/react-common';
import React from 'react';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import { FormattedMessage, defineMessages } from 'react-intl';

import { configureStore } from 'store';
import { LanguageProvider } from './language-provider';
import { translationMessages } from '../../i18n';

describe('<LanguageProvider />', () => {
  let store: any;

  beforeAll(() => {
    store = configureStore({}, setHistory({}));
  });

  it('should render the default language messages', () => {
    const messages = defineMessages({
      someMessage: {
        id: 'some.id',
        defaultMessage: 'This is some default message',
      },
    });
    const renderedComponent = shallow(
      <Provider store={store}>
        <LanguageProvider messages={translationMessages}>
          <FormattedMessage {...messages.someMessage} />
        </LanguageProvider>
      </Provider>
    );
    expect(
      renderedComponent.contains(<FormattedMessage {...messages.someMessage} />)
    ).toEqual(true);
  });
});
