import { connect } from 'react-redux';
import { changeLocaleAction } from 'shared/language/locale-toggle/usecases/change-locale.usecase';
import { localeSelector } from 'shared/language/language.selectors';

import { LocaleToggle } from './locale-toggle';
import { appLocales } from '../../i18n';
import { messages } from './messages/locale-messages';

function mapStateToProps(state: any): any {
  return {
    locale: localeSelector(state),
    appLocales,
    messages,
  };
}

export const mapDispatchToProps = {
  changeLocaleAction,
};

export const LocaleToggleContainer: any = connect(
  mapStateToProps,
  mapDispatchToProps
)(LocaleToggle);
