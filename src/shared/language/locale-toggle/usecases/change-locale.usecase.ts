import { createAction } from 'redux-actions';

import { reducer } from '../../language.reducer';

const postfix = `/${reducer.sliceName}/app`;

export const CHANGE_LOCALE: string = `CHANGE_LOCALE${postfix}`;
export const changeLocaleAction = createAction(CHANGE_LOCALE);

export const reduceHandlers = {
  [CHANGE_LOCALE]: (state: any, action: any) => {
    return {
      ...state,
      locale: action.payload,
    };
  },
};

reducer.addActionReducerMap(reduceHandlers);
