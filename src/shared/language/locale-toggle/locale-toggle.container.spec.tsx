import { Provider } from 'react-redux';
import { setHistory } from '@dealersocket/react-common';
import { ThemeProvider } from '@dealersocket/ds-ui-react/theme/ThemeProvider';
import { theme } from '@dealersocket/ds-ui-react/theme';
import { shallow } from 'enzyme';
import React from 'react';
import { configureStore } from 'store';
import {
  LocaleToggleContainer,
  mapDispatchToProps,
} from './locale-toggle.container';
import { LanguageProvider } from '../language-provider/language-provider';

import { translationMessages } from '../../i18n';

describe('<LocaleToggleContainer />', () => {
  let store: any;

  beforeAll(() => {
    store = configureStore({}, setHistory({}));
  });

  it('should render the default language messages', () => {
    const renderedComponent = shallow(
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <LanguageProvider messages={translationMessages}>
            <LocaleToggleContainer />
          </LanguageProvider>
        </ThemeProvider>
      </Provider>
    );
    expect(renderedComponent.contains(<LocaleToggleContainer />)).toEqual(true);
  });

  describe('mapDispatchToProps', () => {
    describe('changeLocaleAction', () => {
      it('should be injected', () => {
        expect(mapDispatchToProps.changeLocaleAction).toBeDefined();
      });
    });
  });
});
