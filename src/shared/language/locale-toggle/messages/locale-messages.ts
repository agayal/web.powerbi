/*
 * LocaleToggle Messages
 *
 * This contains all the text for the LanguageToggle component.
 */
import { defineMessages } from 'react-intl';
import { appLocales } from '../../../i18n';

export function getLocaleMessages(locales: any): any {
  return locales.reduce(
    (messages: any, locale: string) => ({
      ...messages,
      [locale]: {
        id: `web.app.template.containers.LocaleToggle.${locale}`,
        defaultMessage: `${locale}`,
      },
    }),
    {}
  );
}

export const messages = defineMessages(getLocaleMessages(appLocales));
