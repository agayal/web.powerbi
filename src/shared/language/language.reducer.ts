import { createAndMergeSliceReducer } from '@dealersocket/react-common';

import { DEFAULT_LOCALE } from 'shared/i18n';

const sliceName = 'languageSlice';

export interface LanguageSliceType {
  locale: string;
}

const initialState: LanguageSliceType = {
  locale: DEFAULT_LOCALE,
};

export const reducer = createAndMergeSliceReducer(sliceName, initialState);
