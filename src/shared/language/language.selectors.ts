import { createSelector } from 'reselect';
import { reducer, LanguageSliceType } from './language.reducer';

/**
 * Direct selector to the languageToggle state domain
 */
const languageSliceSelector = (state: any): any => state[reducer.sliceName];

/**
 * Select the language locale
 */
export const localeSelector = createSelector(
  languageSliceSelector,
  (slice: LanguageSliceType) => slice.locale
);

export const testPort = {
  languageSliceSelector,
};
