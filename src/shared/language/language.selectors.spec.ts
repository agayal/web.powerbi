import { testPort } from './language.selectors';

describe('languageSliceSelector', () => {
  const globalSelector = testPort.languageSliceSelector;
  it('should select the global state', () => {
    const globalState = {};
    const mockedState = {
      languageSlice: globalState,
    };
    expect(globalSelector(mockedState)).toEqual(globalState);
  });
});
