import React from 'react';
import { CommonDialog } from '@dealersocket/ds-ui-react/CommonDialog';
import { Button } from '@dealersocket/ds-ui-react/Button';
import { reduxDialog } from '@dealersocket/react-common';

export const CONFIRM_DIALOG = 'CONFIRM_DIALOG';

interface ConfirmDialogProps {
  isOpen: boolean;
  message: string;
  noLabel: string;
  yesLabel: string;
  noAction: () => void;
  yesAction: () => void;
  onRequestClose: (event?: any) => void;
}

const ConfirmDialogComponent = (
  props: ConfirmDialogProps
): React.ReactElement<ConfirmDialogProps> => {
  const {
    isOpen,
    message,
    noLabel,
    yesLabel,
    noAction,
    yesAction,
    onRequestClose,
  } = props;

  const onNoClick = (): void => {
    if (noAction) {
      noAction();
    }
    onRequestClose();
  };

  const onYesClick = (): void => {
    // console.log('onYesClick');
    yesAction();
    onRequestClose();
  };

  const styles = {
    button: {
      margin: '10px',
    },
  };

  const actions = [
    <Button data-e2e="noLabel" onClick={onNoClick}>
      {noLabel}
    </Button>,
    <Button
      data-e2e="yesLabel"
      style={styles.button}
      color="primary"
      onClick={onYesClick}
    >
      {yesLabel}
    </Button>,
  ];

  return (
    <CommonDialog
      actions={actions}
      modal={false}
      open={isOpen}
      onRequestClose={onRequestClose}
    >
      {message}
    </CommonDialog>
  );
};

ConfirmDialogComponent.defaultProps = {
  message: 'Confirm',
  noLabel: 'No',
  yesLabel: 'Yes',
  noAction: null,
};

export const ConfirmDialog = reduxDialog({
  name: CONFIRM_DIALOG,
})(ConfirmDialogComponent);
