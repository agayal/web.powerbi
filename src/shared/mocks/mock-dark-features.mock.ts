import { DarkFeaturePermissionsType } from '../state/dark-feature-permissions/dark-feature-permissions.reducer';

export const mockDarkFeatures: DarkFeaturePermissionsType = {
  PowerBIDarkFeature: false,
};
