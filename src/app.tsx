import Axios from 'axios';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import { createHashHistory } from 'history';
import {
  axiosHelper,
  getHistory,
  isMock,
  lifecycle,
  LifecycleProvider,
  setAppSettings,
  setHistory,
} from '@dealersocket/react-common';

import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';

import { configureStore } from 'store';
import { theme } from '@dealersocket/ds-ui-react/theme';
import { ThemeProvider } from '@dealersocket/ds-ui-react/theme/ThemeProvider';
import { AppRoot } from 'shared/app-root/app-root.component';
import './index.scss';
import { translationMessages } from 'shared/i18n';
import { LanguageProvider } from 'shared/language/language-provider/language-provider';

export function main(appId: string, appSettings: any): undefined {
  setAppSettings(appSettings);
  const appAxios = Axios.create();
  axiosHelper.setAxios(appAxios);

  // Uncomment condition to exclude mock from the production build
  // if (process.env.NODE_ENV === 'development') {
  if (isMock) {
    // Use mock data
    require('./mock-adapter').mock(appAxios); // eslint-disable-line global-require
  }
  // }

  const afterLogin = (user: any, returnHash?: string): void => {
    getHistory().replace(returnHash || '/');
  };

  // lifecycle.init must be called before before the hashHistory is created.
  // redux-oidc was designed to work with browserHistory and not with hashHistory.
  // Therefore lifecycle.init intercepts the hash route '#id_token' before the hashHistory is created.
  // Once created the hash will change to '#/id_token' which could not be processed by the userManager.
  lifecycle.init({ appId, appSettings, afterLogin });

  const history = setHistory(createHashHistory());

  // Create redux store with history
  const initialState: any = {};
  // const store: any = configureStore(initialState, history, injectedHelpers);
  const store: any = configureStore(initialState, history);

  lifecycle.setStore(store);

  const MOUNT_NODE = document.getElementById('root');
  if (!MOUNT_NODE) {
    return;
  }

  const render = (): void => {
    ReactDOM.render(
      <Provider store={store}>
        <LifecycleProvider appId={appId} appSettings={appSettings}>
          <ThemeProvider theme={theme}>
            <LanguageProvider messages={translationMessages}>
              <ConnectedRouter history={history}>
                <AppRoot />
              </ConnectedRouter>
            </LanguageProvider>
          </ThemeProvider>
        </LifecycleProvider>
      </Provider>,
      MOUNT_NODE
    );
  };

  if (process.env.NODE_ENV === 'development') {
    if (module.hot) {
      module.hot.accept('./shared/app-root/app-root.component', () => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE);
        render();
      });
    }
  }

  render();
}
