import MockAdapter from 'axios-mock-adapter';

/* eslint-disable global-require */
export function mock(appAxios: any): any {
  const axiosMock = new MockAdapter(appAxios, { delayResponse: 800 });

  require('area/report/mock/report-access-token.mock-api').mockApi(axiosMock);
  require('area/report/mock/report-list.mock-api').mockApi(axiosMock);
  require('area/report/mock/report-embed.mock-api').mockApi(axiosMock);

  axiosMock.onAny().passThrough();
}
/* eslint-enable global-require */
