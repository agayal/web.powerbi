/**
 * Create the store with asynchronously loaded reducers
 */

import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';
import { initMergeSaga, storeHelper } from '@dealersocket/react-common';

import { createRootReducer } from './reducers';

export function configureStore(initialState: any, history: any): any {
  // create the saga middleware
  const sagaMiddleware = createSagaMiddleware();

  // Create the store with two middlewares
  // 1. routerMiddleware: Syncs the location/URL path to the state
  // 2. sagaMiddleware: enables redux-saga
  const middlewares = [routerMiddleware(history), sagaMiddleware];

  const enhancers = [applyMiddleware(...middlewares)];

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle */
  const win: any = window;
  const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof win === 'object' &&
    win.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? win.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      : compose;
  /* eslint-enable */

  const store: any = createStore(
    createRootReducer(),
    initialState,
    composeEnhancers(...enhancers)
  );
  // Extensions
  store.asyncReducers = {}; // Async reducer registry
  store.sagaMiddleware = sagaMiddleware;

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      import('./reducers').then((reducerModule) => {
        const newCreateRootReducer = reducerModule.createRootReducer;
        const newRootReducer = newCreateRootReducer(store.asyncReducers);

        store.replaceReducer(newRootReducer);
      });
    });
  }

  initMergeSaga(sagaMiddleware);
  storeHelper.init(store, createRootReducer);

  return store;
}
