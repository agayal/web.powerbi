import { axiosResult, getAppSettings } from '@dealersocket/react-common';
import globToRe from 'glob-to-regexp';
import accessTokenResponse from './report-access-token.json';

export function mockApi(axiosMock: any): any {
  axiosMock
    .onGet(
      globToRe(`${getAppSettings().globalApiUrl}/powerbi/accessTokenResults?*`)
    )
    .reply(() => {
      return axiosResult(accessTokenResponse);
    });
}
