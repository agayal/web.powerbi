import { axiosResult, getAppSettings } from '@dealersocket/react-common';
import globToRe from 'glob-to-regexp';
import reportListResponse from './report-list.json';

export function mockApi(axiosMock: any): any {
  axiosMock
    .onGet(
      globToRe(`${getAppSettings().globalApiUrl}/powerbi/reportListResults?*`)
    )
    .reply(() => {
      return axiosResult(reportListResponse);
    });
}
