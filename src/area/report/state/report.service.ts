import { httpGet } from '../../../shared/utils/api-request';

export async function getAccessToken(): Promise<any> {
  const headers = {
    // accessToken: accessTokenSelector(state)
    // BB access token as a key value
  };
  return httpGet(`/powerbi/accessTokenResults?*`, undefined, headers); // return azure access token
}

export async function getReportList(): Promise<any> {
  const headers = {
    // BB access token as a key value
    // azure access token //if not ok than call getAccessToken
  };
  return httpGet(`/powerbi/reportListResults?*`, undefined, headers); // list of reports
}

export async function getEmbeddedReport(): Promise<any> {
  const headers = {
    // BB access token as a key value
    // azure access token //if not ok than call getAccessToken
  };
  return httpGet(`/powerbi/embedReportResults?*`, undefined, headers); // embed report details
}
