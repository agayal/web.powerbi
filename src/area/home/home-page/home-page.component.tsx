import React from 'react';
import { ReportHomePage } from '../../report/view/report-home.component';

interface HomePageProps {
  accessToken: string;
  dealershipId?: string;
  releasePool?: string;
  setNavTitleAction: (title: string) => void;
}

export class HomePage extends React.Component<HomePageProps, any> {
  constructor(...args: any) {
    super(...args);
  }

  render(): React.ReactElement<HomePageProps> {
    const { dealershipId, releasePool, accessToken } = this.props;
    return (
      <div>
        <div style={{ padding: 20 }}>
          <div>dealershipId: {dealershipId}</div>
          <div>releasePool: {releasePool}</div>
          <div>accessToken: {accessToken}</div>
        </div>
        <div />
      </div>
    );
  }
}
