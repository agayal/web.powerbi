import { mountWithProviders } from '@dealersocket/ds-ui-react/test-utils';
import React from 'react';
import { setHistory } from '@dealersocket/react-common';
import { Provider } from 'react-redux';
import { HomePageContainer } from './home-page.container';
import { configureStore } from '../../../store';
import { HomePage } from './home-page.component';

describe('HomePageContainer', () => {
  const props: any = {};
  const store = configureStore({}, setHistory({}));

  it('should render a HomePage', () => {
    const wrapper = mountWithProviders(
      <Provider store={store}>
        <HomePageContainer {...props} />
      </Provider>
    );
    expect(wrapper.find(HomePage).length).toEqual(1);
  });
});
