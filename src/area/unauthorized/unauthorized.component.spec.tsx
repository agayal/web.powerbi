import React from 'react';
import { mountWithProviders } from '@dealersocket/ds-ui-react/test-utils';
import AlertIcon from 'mdi-react/AlertIcon';
import { Text } from '../../shared/components/text/text.component';
import { UnauthorizedPage } from './unauthorized.component';

describe('<UnauthorizedPage>', () => {
  const rootStyles = 'rootStyles';
  const titleStyles = 'titleStyles';
  const descriptionStyles = 'descriptionStyles';
  const props: any = {
    classes: {
      root: rootStyles,
      title: titleStyles,
      description: descriptionStyles,
    },
  };

  it('should render properly', () => {
    const testProps = {
      ...props,
    };

    const wrapper = mountWithProviders(<UnauthorizedPage {...testProps} />);
    expect(wrapper.find(Text).length).toBe(1);
    expect(wrapper.find(AlertIcon).length).toBe(1);
  });
});
