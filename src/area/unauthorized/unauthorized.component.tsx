/**
 * UnauthorizedPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import React from 'react';
import { withStyles, StyleRules } from '@material-ui/core/styles';
import { TypographyVariants } from '@dealersocket/ds-ui-react/Typography';
import AlertIcon from 'mdi-react/AlertIcon';
import { Text } from '../../shared/components/text/text.component';

interface InternalProps {
  classes: any;
}

function UnauthorizedPageComp(
  props: InternalProps
): React.ReactElement<InternalProps> {
  const { classes } = props;
  return (
    <article className={classes.root}>
      <AlertIcon className={classes.icon} />
      <Text
        variant={TypographyVariants.H4}
        id="unauthorized-page.apology"
        defaultMessage="_unauthorized-page.apology_"
        className={classes.description}
      />
    </article>
  );
}

const styles: StyleRules = {
  root: {
    textAlign: 'center',
    marginTop: 150,
  },

  title: {
    marginTop: 15,
  },
  icon: {
    width: 100,
    height: 100,
  },
  description: {
    marginTop: 15,
  },
};

export const UnauthorizedPage = withStyles(styles)(UnauthorizedPageComp);
