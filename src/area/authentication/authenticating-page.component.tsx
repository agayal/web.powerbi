/**
 * AuthenticatingPage
 *
 * This is the page we show when the user's authentication is processing
 */

import React from 'react';

export function AuthenticatingPage(): React.ReactElement<void> {
  return (
    <article>
      <h1>Authenticating...</h1>
    </article>
  );
}
