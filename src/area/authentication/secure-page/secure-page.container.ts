import { connect } from 'react-redux';
import { setNavTitleAction } from 'shared/nav/set-nav-title.usecase';
import { SecurePage } from './secure-page.component';

function mapStateToProps(state: any): any {
  return {
    user: state.oidc.user,
  };
}

const mapDispatchToProps = {
  setNavTitleAction,
};

export const SecurePageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SecurePage);
