import React from 'react';
import { logout } from '@dealersocket/react-common';
import { Button } from '@dealersocket/ds-ui-react/Button';

interface SecurePageProps {
  user: any;
}

export const SecurePage = (
  props: SecurePageProps
): React.ReactElement<void> => {
  const { user } = props;

  return (
    <div>
      <div>{JSON.stringify(user, null, 2)}</div>
      <Button data-e2e="noLabel" id="logout" color="primary" onClick={logout}>
        {'Logout'}
      </Button>
    </div>
  );
};
