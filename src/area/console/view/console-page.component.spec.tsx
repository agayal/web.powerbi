import React from 'react';
import { mountWithProviders } from '@dealersocket/ds-ui-react/test-utils';
import { AppHeader } from '../../../shared/components/app-header';
import { ConsolePage, ConsolePageProps } from './console-page.component';

describe('<ConsolePage>', () => {
  const props: ConsolePageProps = {
    classes: {},
  };

  it('should have the correct title', () => {
    const wrapper = mountWithProviders(<ConsolePage {...props} />);
    expect(wrapper.find(AppHeader).prop('title')).toEqual('console.page-title');
  });
});
