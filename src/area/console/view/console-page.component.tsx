import React from 'react';
import { TypographyVariants } from '@dealersocket/ds-ui-react/Typography';
import { withStyles } from '@material-ui/core/styles';
import { AppHeader } from '_/shared/components/app-header';
import { Text } from '_/shared/components/text/text.component';

export interface ConsolePageProps {
  classes: any;
}

export function ConsolePageComp({
  classes,
}: ConsolePageProps): React.ReactElement<ConsolePageProps> {
  return (
    <div>
      <AppHeader title="console.page-title" />
      <Text
        variant={TypographyVariants.Body2}
        id="shared.insert-content"
        defaultMessage="_Insert Content_"
        className={classes.bodyMargin}
      />
    </div>
  );
}

const styles = {
  bodyMargin: {
    marginTop: 40,
    marginLeft: 40,
  },
};

export const ConsolePage = withStyles(styles)(ConsolePageComp);
