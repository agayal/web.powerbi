import { connect } from 'react-redux';
import { ConsolePage } from './console-page.component';

function mapStateToProps(/* state: any */): any {
  return {
    // releasePool: releasePoolSelector(state),
  };
}

const mapDispatchToProps = {
  // setNavTitleAction,
};

export const ConsolePageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConsolePage);
export const testPort = {
  mapStateToProps,
};
