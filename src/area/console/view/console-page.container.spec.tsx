import { mountWithProviders } from '@dealersocket/ds-ui-react/test-utils';
import React from 'react';
import { Provider } from 'react-redux';
import { setHistory } from '@dealersocket/react-common';
import { ConsolePageContainer, testPort } from './console-page.container';
import { ConsolePage } from './console-page.component';
import { configureStore } from '../../../store';

describe('ConsolePageContainer', () => {
  const props: any = {};
  const store = configureStore({}, setHistory({}));

  it('should render a ConsolePage', () => {
    const component = mountWithProviders(
      <Provider store={store}>
        <ConsolePageContainer {...props} />
      </Provider>
    );
    expect(component.find(ConsolePage).length).toEqual(1);
  });

  it('mapStateToProps should contain expected properties', () => {
    // const mockState = {};
    const result = testPort.mapStateToProps(/* mockState */);
    const expected = {};
    expect(result).toEqual(expected);
  });
});
