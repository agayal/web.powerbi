/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import React from 'react';

export const NotFoundPage = (): React.ReactElement<void> => (
  <article>
    <h1>Page Not Found!</h1>
    <p>
      <a href="/">[home]</a>
    </p>
  </article>
);
