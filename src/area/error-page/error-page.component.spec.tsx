import React from 'react';
import { mountWithProviders } from '@dealersocket/ds-ui-react/test-utils';
import EngineOffIcon from 'mdi-react/EngineOffIcon';
import { Text } from '../../shared/components/text/text.component';
import { ErrorPage } from './error-page.component';

describe('<ErrorPage>', () => {
  const rootStyles = 'rootStyles';
  const titleStyles = 'titleStyles';
  const descriptionStyles = 'descriptionStyles';
  const props: any = {
    classes: {
      root: rootStyles,
      title: titleStyles,
      description: descriptionStyles,
    },
  };

  it('should render properly', () => {
    const testProps = {
      ...props,
    };

    const wrapper = mountWithProviders(<ErrorPage {...testProps} />);
    expect(wrapper.find(Text).length).toBe(1);
    expect(wrapper.find(EngineOffIcon).length).toBe(1);
  });
});
