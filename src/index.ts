import Axios from 'axios';
import { main } from './app';

const APP_ID = 'analytics2';

Axios.get('app.settings.json').then((response) => {
  const appSettings = response.data;
  main(APP_ID, Object.freeze(appSettings));
});
