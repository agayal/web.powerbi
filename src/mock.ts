import MockAdapter from 'axios-mock-adapter';

/* eslint-disable global-require */
export function mock(appAxios: any): any {
  const axiosMock = new MockAdapter(appAxios, { delayResponse: 800 });

  // require('area/dallas-class/mock/dallas-students.mock-api').mockApi(axiosMock);

  axiosMock.onAny().passThrough();
}
/* eslint-enable global-require */
