/**
 * Combine all reducers in this file and export the combined reducers.
 * If we were to do this in store.ts, reducers wouldn't be hot reloadable.
 */
import { combineReducers, Reducer } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { connectRouter } from 'connected-react-router';
import { commonReducers, getHistory } from '@dealersocket/react-common';
import { reducer as languageReducer } from 'shared/language/language.reducer';
import { reducer as navReducer } from 'shared/nav/nav.reducer';
import { reducer as toastrReducer } from 'react-redux-toastr';

/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export function createRootReducer(asyncReducers?: any): Reducer<any> {
  let sliceReducers = {
    form: formReducer,
    languageSlice: languageReducer,
    navSlice: navReducer,
    toastr: toastrReducer,
    ...commonReducers,
    ...asyncReducers,
  };

  const history = getHistory();
  if (history) {
    sliceReducers = {
      // 'router' key required by 'connected-react-router'
      router: connectRouter(history),
      ...sliceReducers,
    };
  }

  return combineReducers(sliceReducers);
}
